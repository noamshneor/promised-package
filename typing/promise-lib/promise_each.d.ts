export declare function each(iter: any[], func: (res: any, i?: number, len?: number) => Promise<any>): Promise<any[]>;
