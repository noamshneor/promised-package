export declare function reduce(iter: any[], func: (val: any, cur: number) => Promise<any>, initval?: any): Promise<any>;
