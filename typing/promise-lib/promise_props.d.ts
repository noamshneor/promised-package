interface IPromiseObj {
    [k: string | number]: any;
}
export declare function props(obj: IPromiseObj): Promise<IPromiseObj>;
export {};
