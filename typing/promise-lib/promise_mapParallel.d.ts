export declare function mapParallel(iter: any[], mupper: (val: any, i?: number, len?: number) => Promise<any>): Promise<any[]>;
