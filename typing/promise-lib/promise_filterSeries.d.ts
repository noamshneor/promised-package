export declare function filterSeries(iter: any[], func: (val: any, i?: number, len?: number) => Promise<boolean>): Promise<any[]>;
