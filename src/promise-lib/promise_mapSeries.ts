export async function mapSeries(
    iter: any[],
    mupper: (val: any, i?: number, len?: number) => Promise<any>
): Promise<any[]> {
    const result = [];
    for (let i = 0; i < iter.length; i++) {
        result.push(await mupper(iter[i], i, iter.length));
    }
    return result;
}

// const promiseArr = [Promise.resolve(3), 42, new Promise((resolve) => {
//     setTimeout(resolve, 100, 'foo');
// })];

// console.log(await mapSeries(promiseArr, async (value) => {
//     value += 1;
//     return value;
// }));
