// import Promise from "Bluebird";
export async function filterParallel(
    iter: any[],
    func: (val: any, i?: number, len?: number) => Promise<boolean>
) {
    const promiseArr = [];
    for (let i = 0; i < iter.length; i++) {
        const value = await iter[i];
        promiseArr.push(func(value, i, iter.length));
    }
    const ans = [];
    const boolArr = await Promise.all(promiseArr);
    for (let i = 0; i < boolArr.length; i++) {
        if (boolArr[i]) {
            ans.push(await iter[i]);
        }
    }
    return ans;
}

// const promiseArr = [Promise.resolve(3), 42, new Promise((resolve) => {
//     setTimeout(resolve, 100, 'foo');
// })];

// console.log(await filterParallel(promiseArr, async (value) => {
//     await Promise.delay(1000);
//     // console.log(typeof value);
//     return typeof value === 'number';
// }));
