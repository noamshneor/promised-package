export async function reduce(
    iter: any[],
    func: (val: any, cur: number) => Promise<any>,
    initval?: any
) {
    let val = (await initval) || (await iter.shift());
    for (let cur of iter) {
        cur = await cur;
        val = await func(val, cur);
    }
    return val;
}
