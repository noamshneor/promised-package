// import Promise from "bluebird";

export async function each(
    iter: any[],
    func: (res: any, i?: number, len?: number) => Promise<any>
) {
    const result = [];
    for (let i = 0; i < iter.length; i++) {
        result.push(await iter[i]);
        await func(result[i], i, iter.length);
    }
    return result;
}

// const promiseArr = [Promise.resolve(3), 42, new Promise((resolve, reject) => {
//     setTimeout(resolve, 100, 'foo');
// })];

// console.log(await each(promiseArr, async (value) => {
//     console.log(await Promise.delay(1000, value));
// }));
