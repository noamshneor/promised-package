export async function mapParallel(
    iter: any[],
    mupper: (val: any, i?: number, len?: number) => Promise<any>
) {
    const promiseArr = [];
    for (let i = 0; i < iter.length; i++) {
        promiseArr.push(mupper(iter[i], i, iter.length));
    }
    return await Promise.all(promiseArr);
}

// const promiseArr = [Promise.resolve(3), 42, new Promise((resolve) => {
//     setTimeout(resolve, 100, 'foo');
// })];

// console.log(await mapParallel(promiseArr, async (value) => {
//     value += 1;
//     await Promise.delay(1000);
//     console.log(value);
//     return value;
// }));
