export function race(iter: any[]) {
    return new Promise((resolve) => {
        for (const item of iter) {
            if (item instanceof Promise) {
                item.then((res) => resolve(res)); //after you finish do resolve and end the function
            } else {
                resolve(item);
            }
        }
    });
}
