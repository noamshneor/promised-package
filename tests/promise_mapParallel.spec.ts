import { performance } from "perf_hooks";
import { expect } from "chai";
import { mapParallel } from "../src/promise-lib/promise_mapParallel";
import { delay } from "../src/promise-lib/promise_delay";


describe("Promise mapParallel module", () => {
    context("#mapParallel", () => {
        it("should exist", () => {
            expect(mapParallel).to.be.a("function");
            expect(mapParallel).to.be.instanceOf(Function);
        });
        it("should return array after change", async () => {
            const inputs = [2, 3, 4, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return item * 2;
            };
            const outputs = await mapParallel(inputs, mapper);

            expect(outputs).to.deep.equal([4, 6, 8, 10]);

            // const promiseArr: (Promise<any> | any)[] = [Promise.resolve(3), 42, new Promise((resolve) => {
            //     setTimeout(resolve, 500, "foo");
            // })];
            // const valArr = await mapParallel(promiseArr, async (value) => {
            //     await delay(500);
            //     return ((await value) + 1);
            // });
            // expect(valArr).to.eql([4, 43, "foo1"]);
        });
        it("should run in parallel", async () => {
            const inputs = [2, 3, 4, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return item * 2;
            };
            const start = performance.now();
            await mapParallel(inputs, mapper);
            const end = performance.now();
            const ms = end - start;
            // console.log("perf:", ms);

            expect(ms).to.lessThan(400);
        });
    });
});