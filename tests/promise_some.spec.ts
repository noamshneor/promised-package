import { expect } from "chai";
import { some } from "../src/promise-lib/promise_some";

describe("Promise some module", () => {
    context("#some", () => {
        it("should exist", () => {
            expect(some).to.be.a("function");
            expect(some).to.be.instanceOf(Function);
        });
        it("should return val that first resolved", async () => {
            const promise1 = new Promise((resolve) => {
                setTimeout(resolve, 200, 3);
            });
            const promise2 = new Promise((resolve) => {
                setTimeout(resolve, 300, 42);
            });
            const promise3 = new Promise((resolve) => {
                setTimeout(resolve, 100, "foo");
            });
            const val = await some([promise1, promise2, promise3], 2);
            expect(val).to.eql(["foo", 3]);
        });
    });
});