import { performance } from "perf_hooks";
import { expect } from "chai";
import { mapSeries } from "../src/promise-lib/promise_mapSeries";
import { delay } from "../src/promise-lib/promise_delay";



describe("Promise mapSeries module", () => {
    context("#mapSeries", () => {
        it("should exist", () => {
            expect(mapSeries).to.be.a("function");
            expect(mapSeries).to.be.instanceOf(Function);
        });
        it("should return array after change", async () => {
            const inputs = [2, 3, 4, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return item * 2;
            };
            const outputs = await mapSeries(inputs, mapper);

            expect(outputs).to.deep.equal([4, 6, 8, 10]);


            // const promiseArr: (Promise<any> | any)[] = [Promise.resolve(3), 42, new Promise((resolve) => {
            //     setTimeout(resolve, 500, "foo");
            // })];
            // const valArr = await mapSeries(promiseArr, async (value) => {
            //     await delay(500);
            //     return ((await value) + 1);
            // });
            // expect(valArr).to.eql([4, 43, "foo1"]);
        });
        it("should run in sequence", async () => {
            const inputs = [2, 3, 4, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return item * 2;
            };
            const start = performance.now();
            await mapSeries(inputs, mapper);
            const end = performance.now();
            const ms = end - start;
            //console.log("perf:", ms);

            expect(ms).to.greaterThanOrEqual(400);
        });
    });
});