import { expect } from "chai";
import { all } from "../src/promise-lib/promise_all";

describe("Promise all module", () => {
    context("#all", () => {
        it("should exist", () => {
            expect(all).to.be.a("function");
            expect(all).to.be.instanceOf(Function);
        });
        it("should return resolve array", async () => {
            const promiseArr: (Promise<any> | any)[] = [Promise.resolve(3), 42, new Promise((resolve) => {
                setTimeout(resolve, 500, "foo");
            })];
            expect(await all(promiseArr)).to.eql([3, 42, "foo"]);
        });
        // it("should throw error - array input", async () => {
        //     try {
        //         await all([1,2,3]);
        //     } catch (err: any) {
        //         expect(err.message).to.equal("function 'all' get array as param");
        //     }
        // });
    });
});