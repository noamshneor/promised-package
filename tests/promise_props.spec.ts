import { expect } from "chai";
import { props } from "../src/promise-lib/promise_props";

describe("Promise props module", () => {
    context("#props", () => {
        it("should exist", () => {
            expect(props).to.be.a("function");
            expect(props).to.be.instanceOf(Function);
        });
        it("should return object of resolved values", async () => {
            const promise1 = Promise.resolve(3);
            const promise2 = 42;
            const promise3 = new Promise((resolve) => {
                setTimeout(resolve, 100, "foo");
            });
            const obj = await props({ promise1, promise2, promise3 });
            expect(Object.values(obj)).to.eql([3, 42, "foo"]);
        });
    });
});