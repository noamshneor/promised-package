import { expect } from "chai";
import { reduce } from "../src/promise-lib/promise_reduce";

describe("Promise reduce module", () => {
    context("#reduce", () => {
        it("should exist", () => {
            expect(reduce).to.be.a("function");
            expect(reduce).to.be.instanceOf(Function);
        });
        it("should return value of the reduce", async () => {
            const promiseArr: any[] = [Promise.resolve(3), 42, new Promise((resolve) => {
                setTimeout(resolve, 500, 5);
            })];
            const val = await reduce(promiseArr, async (pre, cur) => {
                return (cur + pre);
            }, 0);
            expect(val).to.eql(50);
        });
    });
});