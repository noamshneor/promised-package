import { performance } from "perf_hooks";
import { expect } from "chai";
import { filterParallel } from "../src/promise-lib/promise_filterParallel";
import { delay } from "../src/promise-lib/promise_delay";



describe("Promise filterParallel module", () => {
    context("#filterParallel", () => {
        it("should exist", () => {
            expect(filterParallel).to.be.a("function");
            expect(filterParallel).to.be.instanceOf(Function);
        });
        it("should return filtered array", async () => {
            const inputs = [2, "3", "a", 4, true, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return typeof item === "number";
            };
            const outputs = await filterParallel(inputs, mapper);

            expect(outputs).to.deep.equal([2, 4, 5]);

            // const promiseArr: (Promise<any> | any)[] = [Promise.resolve(3), 42, new Promise((resolve) => {
            //     setTimeout(resolve, 500, "foo");
            // })];
            // const ans = await filterParallel(promiseArr, async (value) => {
            //     await delay(500);
            //     return typeof value === "number";
            // });
            // expect(ans).to.eql([3, 42]);
        });
        it("should run in parallel", async () => {
            const inputs = [2, "3", "a", 4, true, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return typeof item === "number";
            };
            const start = performance.now();
            await filterParallel(inputs, mapper);
            const end = performance.now();
            const ms = end - start;
            // console.log("perf:", ms);

            expect(ms).to.lessThan(600);
        });
    });
});