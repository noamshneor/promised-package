import { performance } from "perf_hooks";
import { expect } from "chai";
import { filterSeries } from "../src/promise-lib/promise_filterSeries";
import { delay } from "../src/promise-lib/promise_delay";


describe("Promise filterSeries module", () => {
    context("#filterSeries", () => {
        it("should exist", () => {
            expect(filterSeries).to.be.a("function");
            expect(filterSeries).to.be.instanceOf(Function);
        });
        it("should return filtered array", async () => {
            const inputs = [2, "3", "a", 4, true, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return typeof item === "number";
            };
            const outputs = await filterSeries(inputs, mapper);

            expect(outputs).to.deep.equal([2, 4, 5]);
        });
        it("should run in sequence", async () => {
            const inputs = [2, "3", "a", 4, true, 5];
            const mapper = async (item: number) => {
                await delay(100);
                return typeof item === "number";
            };
            const start = performance.now();
            await filterSeries(inputs, mapper);
            const end = performance.now();
            const ms = end - start;
            // console.log("perf:", ms);

            expect(ms).to.greaterThanOrEqual(600);
        });
    });
});