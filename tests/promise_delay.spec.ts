import { expect } from "chai";
import { delay } from "../src/promise-lib/promise_delay";

describe("Promise delay module", () => {
    context("#delay", () => {
        it("should exist", () => {
            expect(delay).to.be.a("function");
            expect(delay).to.be.instanceOf(Function);
        });
        it("should return resolve val after 300 ms", async () => {
            const ms = 300;
            const startMs = Date.now();
            await delay(ms);
            const curMs = Date.now();
            expect(curMs - startMs).to.gte(ms);
        });
        it("should return resolve val after 1000 ms", async () => {
            const ms = 1000;
            const startMs = Date.now();
            await delay(ms);
            const curMs = Date.now();
            expect(curMs - startMs).to.gte(ms);
        });
    });
});